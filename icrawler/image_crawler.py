# crawler for bing and google
import os
import shutil
import logging
import sys
from datetime import date
import time
from keywordlist import keys
from icrawler.examples import GoogleImageCrawler
from icrawler.examples import BingImageCrawler

image_download_count=800
offset=0
feeder_threads=1
parser_threads=1
downloader_threads=4

def google(location, keyword):
    google_crawler = GoogleImageCrawler(location, log_level=logging.INFO)
    google_crawler.crawl(keyword, offset, image_download_count, feeder_threads, parser_threads, downloader_threads)
    

def bing(location, keyword):
    bing_crawler = BingImageCrawler(location)
    bing_crawler.crawl(keyword, offset, image_download_count, feeder_threads, parser_threads, downloader_threads)
    
    
def baidu(location, keyword):
        baidu_crawler = BaiduImageCrawler(location)
        baidu_crawler.crawl(keyword, offset, image_download_count+100, feeder_threads, parser_threads, downloader_threads)


def main():
    for word in keys:
	dira='images/'+word
	google(dira,word)
	bing(dira,word)
	baidu(dira,word)
    #os.system("find . -type f ! -name "*.*" -delete")#delets all files without extension
		      
 
if  __name__ == '__main__':
    main()
